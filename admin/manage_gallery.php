<?php
session_start();
if(!$_SESSION['logged_in']){
    header('Location: login.php');
}
require_once('../includes/connection.php');


$upload_errors = array(
    UPLOAD_ERR_OK   => "No errors",
    UPLOAD_ERR_INI_SIZE => "Larger than upload_max_filesize",
    UPLOAD_ERR_FORM_SIZE => "Larger than form MAX_FILE_SIZE",
    UPLOAD_ERR_PARTIAL => "Partial upload",
    UPLOAD_ERR_NO_FILE => "No file",
    UPLOAD_ERR_NO_TMP_DIR => "No temporary directory",
    UPLOAD_ERR_CANT_WRITE => "Can't write to disc",
    UPLOAD_ERR_EXTENSION => "File upload stopped by extension"
    );

if(isset($_POST['submit'])){

    $tmp_file = $_FILES['file_upload']['tmp_name'];
    $target_file = $_FILES['file_upload']['name'];
    $target_file = basename($target_file);
    $target_file = iconv("utf-8","cp1251",  $target_file);
    $upload_dir = '../uploads/gallery';
    



    if (!file_exists($upload_dir.'/'.$target_file)){
        if(move_uploaded_file($tmp_file, $upload_dir.'/'.$target_file)){
            $message = "File uploaded successfully.";

            $photo_name = basename($_FILES['file_upload']['name']);
            $photo_name = htmlspecialchars($photo_name);
            $photo_name = mysqli_real_escape_string($connection,$photo_name);


    // 2. Perform database query
            $query  = "INSERT INTO gallery_photos (";
            $query .= "  name ";
            $query .= ") VALUES (";
            $query .= "  '{$photo_name}'";
            $query .= ")";

            $result = mysqli_query($connection, $query);

            if ($result) {
        // Success
        // redirect_to("somepage.php");
           // echo "Success!";
            } else {
        // Failure
        // $message = "Subject creation failed";
                die("Database query failed. " . mysqli_error($connection));
            }
        } else {
            $error = $_FILES['file_upload']['error'];
            $message = $upload_errors[$error];
        } 
    } else {
        //$message = "Файл с таким названием уже существует";
    }

    
    



}


?>
<!doctype html>
<html>
<head>
    <title>Галерея</title>
    <link rel='stylesheet' href='css/normalize.css'>
    <link rel='stylesheet' href='css/admin.css'>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <?php include "../includes/admin_navigation.php" ?>


    <div class='admin-main'>
     <h2>Галерея</h2>


     <div class='admin-content admin-content_gallery'>
         <?php if (!empty($message)) {
            echo "<p class='admin-error'>{$message}</p>";
        } ?>


        <?php
        $query = "SELECT * FROM gallery_photos";
        $result = mysqli_query($connection, $query);
        if (!$result){
            die("Something wrong with gallery database");
        }
        while($row = mysqli_fetch_assoc($result)){

            ?>
            <div class='card'>
                <div class='card__image'><img src='../uploads/gallery/<?php echo $row['name']?>'></div>
                <div class='card__descr'>Название файла: <?php echo $row['name'] ?></div>




                <form action='manage_gallery_delete.php' method='post'>
                    <input type='hidden' name='id' value='<?php echo $row["id"] ?>'>
                    <input type='submit' onclick='return confirm("Вы точно хотите удалить этот коллаж?");' value='Удалить коллаж'></form></div>

                    <?php
                }
                $query = '';
                mysqli_free_result($result);
                ?>

                <form class="" action="manage_gallery.php" enctype="multipart/form-data" method="POST">
                    <h3>Добавить коллаж в галерею:</h3>
                    <div>Загружать только в формате .jpg или .png и размером меньше 2МБ. Рекомендуемое соотношение сторон для коллажа: 0.3125 (идеально 1920х600)</div>
                    <input type="hidden" name="MAX_FILE_SIZE" value="2000000"/>
                    <input type="file" name="file_upload"/>

                    <input type="submit" name="submit" value="Загрузить"/>
                </form>

            </div>
        </div>



    </body>
    </html>
    <?php mysqli_close($connection); ?>