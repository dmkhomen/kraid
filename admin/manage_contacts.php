<?php

require_once('../includes/connection.php');




if(isset($_POST['submit'])){

	$id = mysqli_real_escape_string($connection,$_POST['id']);

	$header = htmlspecialchars($_POST['header']);
	$header = mysqli_real_escape_string($connection,$header);

	$info_1 = htmlspecialchars($_POST['info_1']);
	$info_1 = mysqli_real_escape_string($connection,$info_1);


	$info_2 = htmlspecialchars($_POST['info_2']);
	$info_2 = mysqli_real_escape_string($connection,$info_2);

	$query  = "UPDATE contacts ";
	$query .= "SET header='{$header}', info_1='{$info_1}', info_2='{$info_2}' ";
	$query .= "WHERE id='{$id}'";

	$result = mysqli_query($connection, $query);
	if ($result) {

	} else {

		die("Database query failed (title update). " . mysqli_error($connection));
	}
}

if(isset($_POST['delete'])){
	$id = mysqli_real_escape_string($connection,$_POST['id']);

	$query  = "DELETE FROM contacts ";
	$query .= "WHERE id='{$id}' ";
	$query .= "LIMIT 1";

	$result = mysqli_query($connection, $query);
	if ($result) {

	} else {

		die("Database query failed (contact delete). " . mysqli_error($connection));
	}
}


?>
<!doctype html>
<html>
<head>
	<title>Контакты</title>
	<link rel='stylesheet' href='css/normalize.css'>
	<link rel='stylesheet' href='css/admin.css'>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<?php include "../includes/admin_navigation.php" ?>


	<div class='admin-main'>
		<h2>Контакты</h2>
<div class='admin-content admin-content_contacts'>



		
		<!-- <h2>Обновить:</h2> -->

		<?php

		$query = "SELECT * FROM contacts ";
		$result = mysqli_query($connection, $query);
		if (!$result){
			die("Something wrong with contest_block database");
		}
		while($row = mysqli_fetch_assoc($result)){


			if($row['header']=='googlemaps'){
				//echo $row['header'];
				?>	
				<form id='usrform' class="googlemaps-form" action="manage_contacts.php" enctype="multipart/form-data" method="POST">
				<hr/>
					<div>Google Maps координаты</div>
					<div>Откройте maps.google.com, выберите нужное место, щелкните правой кнопкой, выберите "Что здесь?". Во всплывающем окне будут два числа вида 50.419187, 30.477733. Скопируйте их в поля и нажмите "Обновить координаты"</div>
					<input type='hidden' name='header' value="<?php echo $row['header']; ?>">
					<div>Широта (первое число пары)</div>
					<input type='text' name='info_1' value="<?php echo $row['info_1']; ?>">
					<div>Долгота (второе число пары)</div>
					<input type='text' name='info_2' value="<?php echo $row['info_2']; ?>">
					<input type='hidden' name='id' value='<?php echo $row['id']; ?>'>


					<input type="submit" name="submit" value="Обновить координаты"/>
					
				</form>
				
				<?php


			} else {
				

				?>
				<form id='usrform' class="manage-gallery__form" action="manage_contacts.php" enctype="multipart/form-data" method="POST">
				<hr>
					<div>Заголовок контакта</div>
					<input type='text' name='header' value="<?php echo $row['header']; ?>">
					<div>Информация 1 </div>
					<input type='text' name='info_1' value="<?php echo $row['info_1']; ?>">
					<div>Информация 2 </div>
					<input type='text' name='info_2' value="<?php echo $row['info_2']; ?>">
					<input type='hidden' name='id' value='<?php echo $row['id']; ?>'>


					<input type="submit" name="submit" value="Обновить контакт"/>
					<input type="submit" onclick="return confirm('Вы точно хотите удалить этот контакт?');" name="delete" value="Удалить контакт"/>
				</form>
				<?php
			}

			



			
		}
		$query = '';
		mysqli_free_result($result);
		?>

		
		<h3>Добавить новый контакт</h3>
		<form id='usrform_cr' class="manage-gallery__form" action="manage_contacts_create.php" enctype="multipart/form-data" method="POST">
			<div>Заголовок контакта (напр. Редакция)</div>
			<input type='text' name='header' value=''>
			<div>Информация 1 (адрес/имя/телефон)</div>
			<input type='text' name='info_1' value=''>
			<div>Информация 2 (опционально)</div>
			<input type='text' name='info_2' value=''>

			<input type="submit" name="submit" value="Создать новый контакт"/>


		</form>
		

	</div>

	</div>


</body>
</html>
<?php mysqli_close($connection); ?>