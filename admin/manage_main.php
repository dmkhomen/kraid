<?php
session_start();
if(!$_SESSION['logged_in']){
	header('Location: login.php');
}
require_once('../includes/connection.php');

if(isset($_POST['submit'])){

	if(!$_FILES['file_upload']['tmp_name']==''){


		$tmp_file = $_FILES['file_upload']['tmp_name'];
		$target_file = $_FILES['file_upload']['name'];
		$target_file = basename($target_file);
		$target_file = iconv("utf-8","cp1251",  $target_file);
		$upload_dir = '../uploads/main';


		if(move_uploaded_file($tmp_file, $upload_dir.'/'.$target_file)){
			$message = "File uploaded successfully.";

			$photo_name = basename($_FILES['file_upload']['name']);
			$photo_name = htmlspecialchars($photo_name);
			$photo_name = mysqli_real_escape_string($connection,$photo_name);


			$query  = "UPDATE blocks ";
			$query .= "SET image='{$photo_name}' ";
			$query .= "WHERE name='main'";

			$result = mysqli_query($connection, $query);

			if ($result) {

			} else {

				die("Database query failed. " . mysqli_error($connection));
			}
		} else {
			$error = $_FILES['file_upload']['error'];
			$message = $upload_errors[$error];
		} 
	} else {
		$message = "Файл с таким названием уже существует";
	}

    //update des
	if(!$_POST['description']==''){
		$description = htmlspecialchars($_POST['description']);
		$description = mysqli_real_escape_string($connection,$description);

		$query  = "UPDATE blocks ";
		$query .= "SET description='{$description}' ";
		$query .= "WHERE name='main'";

		$result = mysqli_query($connection, $query);
		if ($result) {

		} else {

			die("Database query failed (description update). " . mysqli_error($connection));
		}
	}

//update title

	if(!$_POST['title']==''){
		$title = htmlspecialchars($_POST['title']);
		$title = mysqli_real_escape_string($connection,$title);

		$query  = "UPDATE blocks ";
		$query .= "SET title='{$title}' ";
		$query .= "WHERE name='main'";

		$result = mysqli_query($connection, $query);
		if ($result) {

		} else {

			die("Database query failed (title update). " . mysqli_error($connection));
		}
	}

}


?>
<!doctype html>
<html>
<head>
	<title>Первый блок</title>
	<link rel='stylesheet' href='css/normalize.css'>
	<link rel='stylesheet' href='css/admin.css'>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<?php include "../includes/admin_navigation.php" ?>


	<div class='admin-main'>
		<h2>Первый блок</h2>


		<div class='admin-content'>

			<form id='usrform' class="manage-gallery__form" action="manage_main.php" enctype="multipart/form-data" method="POST">
				

				<?php

				$query = "SELECT * FROM blocks WHERE name='main'";
				$result = mysqli_query($connection, $query);
				if (!$result){
					die("Something wrong with main_block database");
				}
				while($row = mysqli_fetch_assoc($result)){



					?>

					<div>Заголовок</div>
					<input type='text' name='title' value="<?php echo $row['title']; ?>">
					<div>Описание</div>
					<textarea rows="10" cols="80" name="description" form="usrform"><?php echo $row['description']; ?></textarea>
					<div>Картинка, которая используется сейчас - <?php echo $row['image'];?></div>
					<img style='display: block; max-width: 600px; max-height: 400px;' src="../uploads/main/<?php echo $row['image'];?>">
					<input type="hidden" name='MAX_FILE_SIZE' value='2000000'/>
					<input type="file" name="file_upload"/>
					<input type="submit" name="submit" value="Обновить блок"/>




					<?php 
				}
				$query = '';
				mysqli_free_result($result);
				?>


			</form>
		</div>
	</div>

	
<?php include("../includes/mce.php");?>

</body>
</html>
<?php mysqli_close($connection); ?>