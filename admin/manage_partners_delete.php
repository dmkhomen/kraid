<?php
session_start();
if(!$_SESSION['logged_in']){
    header('Location: login.php');
}
require_once("../includes/connection.php");

if (isset($_POST['id'])){
	$id_to_delete = $_POST['id'];
} else {
	//$id_to_delete = null;
	die("no id" . var_dump($_POST));
}
$query = "SELECT * FROM partner_logos ";
$query .= "WHERE id = {$id_to_delete} ";
$result = mysqli_query($connection, $query);
$row = mysqli_fetch_assoc($result);
	

$file_to_unlink = $row['name'];
$file_to_unlink = basename($file_to_unlink);
$file_to_unlink = iconv("utf-8","cp1251",  $file_to_unlink);
unlink("../uploads/partners/" . $file_to_unlink);

$query = "DELETE FROM partner_logos ";
$query .= "WHERE id = {$id_to_delete} ";
$query .= "LIMIT 1";
$result = mysqli_query($connection, $query);
if ($result && mysqli_affected_rows($connection) == 1){
header('Location: manage_partners.php');
} else {
	die("DB query failed. " . mysqli_error($connection));
}