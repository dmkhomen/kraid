<?php
session_start();
if(!$_SESSION['logged_in']){
    header('Location: login.php');
}
require_once('../includes/connection.php');

if(isset($_POST['submit']) && !$_POST['cite']==''){

    $cite = htmlspecialchars($_POST['cite']);
    $cite = mysqli_real_escape_string($connection,$cite);


    $author = htmlspecialchars($_POST['author']);
    $author = mysqli_real_escape_string($connection,$author);



    $query  = "INSERT INTO comments (";
    $query .= " cite, author ";
    $query .= ") VALUES (";
    $query .= " '{$cite}','{$author}'";
    $query .= ")";

    $result = mysqli_query($connection, $query);

    if ($result) {
        // Success
        // redirect_to("somepage.php");
        //echo "Success!";
        header('Location: manage_comments.php');
    } else {
        // Failure
        // $message = "Subject creation failed";
        die("Database query failed. " . mysqli_error($connection));
    }
}
//header('Location: manage_contacts.php');