<?php
session_start();
if(!$_SESSION['logged_in']){
    header('Location: login.php');
}
require_once('../includes/connection.php');




if(isset($_POST['submit'])){

  if($_POST['youtube_link']!=''){
    $youtube_link = $_POST['youtube_link'];
    $youtube_link = str_replace('https://youtu.be/','',$youtube_link);
    $youtube_link = htmlspecialchars($youtube_link);
    $youtube_link = mysqli_real_escape_string($connection,$youtube_link);

    
    



    $query  = "UPDATE youtube_videos ";
    $query .= "SET video_id = '{$youtube_link}' ";
    $query .= "WHERE id=1";
    

    $result = mysqli_query($connection, $query);

    if ($result) {
        // Success
        // redirect_to("somepage.php");
            // echo "Success!";
    } else {
        // Failure
        // $message = "Subject creation failed";
      die("Database query failed. " . mysqli_error($connection));
    }


  }

  
  



}


?>
<!doctype html>
<html>
<head>
  <title>Видео Youtube</title>
  <link rel='stylesheet' href='css/normalize.css'>
  <link rel='stylesheet' href='css/admin.css'>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
  <?php include "../includes/admin_navigation.php" ?>


  <div class='admin-main'>
   <h2>Видео Youtube</h2>
<div class='admin-content'>
   <?php if (!empty($message)) {
    echo "<p class='admin-error'>{$message}</p>";
  } 

  $query = "SELECT * FROM youtube_videos";
  $result = mysqli_query($connection, $query);
  if (!$result){
    die("Something wrong with colorbooks database");
  }
  while($row = mysqli_fetch_assoc($result)){
    echo "<iframe class='youtube__iframe' width='480' height='360'
    src='https://www.youtube.com/embed/{$row["video_id"]}'>
  </iframe>";


}
$query = '';
mysqli_free_result($result);
?>



<form class="manage-gallery__form" action="manage_youtube.php" enctype="multipart/form-data" method="POST">
  <h3>Обновить видео:</h3>
  <p>Запустите видео на youtube, нажмите на нём правой кнопкой, выберите "Копировать URL видео" и вставьте в поле. НЕ вставляйте ссылку из браузера.</p>
        <input type='text' name='youtube_link'>
        <input type="submit" name="submit" value="Обновить видео"/>
      </form>

    </div>

   </div>


  </body>
  </html>
  <?php mysqli_close($connection); ?>