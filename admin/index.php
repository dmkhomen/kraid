<?php
session_start();
if(!$_SESSION['logged_in']){
    header('Location: login.php');
}

?>
<!doctype html>
<html>
<head>
<title>Добро пожаловать</title>
    <link rel='stylesheet' href='css/normalize.css'>
    <link rel='stylesheet' href='css/admin.css'>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

    <?php include "../includes/admin_navigation.php" ?>

    <div class='admin-main'>

    <h2>Добро пожаловать в администраторскую панель</h2>
        <div class='admin-content'>
            
            <p>Используйте меню слева для доступа к разделам.</p>
            <p>Вы можете также <a href='logout.php'>выйти</a></p>
        </div>
    </div>
    

</body>
</html>