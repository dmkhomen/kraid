<?php
session_start();
if(!$_SESSION['logged_in']){
    header('Location: login.php');
}
require_once('../includes/connection.php');




if(isset($_POST['submit'])){

	$id = htmlspecialchars($_POST['id']);
	$id = mysqli_real_escape_string($connection,$id);

	$cite = htmlspecialchars($_POST['cite']);
	$cite = mysqli_real_escape_string($connection,$cite);
	

	$author = htmlspecialchars($_POST['author']);
	$author = mysqli_real_escape_string($connection,$author);
	

	$query  = "UPDATE comments ";
	$query .= "SET cite='{$cite}', author='{$author}' ";
	$query .= "WHERE id='{$id}'";

	$result = mysqli_query($connection, $query);
	if ($result) {

	} else {

		die("Database query failed (title update). " . mysqli_error($connection));
	}
}

if(isset($_POST['delete'])){
	$id = mysqli_real_escape_string($connection,$_POST['id']);

	$query  = "DELETE FROM comments ";
	$query .= "WHERE id='{$id}' ";
	$query .= "LIMIT 1";

	$result = mysqli_query($connection, $query);
	if ($result) {

	} else {

		die("Database query failed (contact delete). " . mysqli_error($connection));
	}
}


?>
<!doctype html>
<html>
<head>
	<title>Отзывы</title>
	<link rel='stylesheet' href='css/normalize.css'>
	<link rel='stylesheet' href='css/admin.css'>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<?php include "../includes/admin_navigation.php" ?>


	<div class='admin-main'>
		<h2>Отзывы</h2>

<div class='admin-content admin-content_comments'>


		
		

		<?php

		$query = "SELECT * FROM comments ";
		$result = mysqli_query($connection, $query);
		if (!$result){
			die("Something wrong with contest_block database");
		}
		while($row = mysqli_fetch_assoc($result)){


			?>
			<form id='usrform' class="manage-gallery__form" action="manage_comments.php" enctype="multipart/form-data" method="POST">
			<hr>
				<div>Текст отзыва.</div>
				<textarea rows="7" cols="80" name="cite"><?php echo $row['cite']; ?></textarea>
				<div>Автор</div>
				<input type='text' name='author' value="<?php echo $row['author']; ?>">


				<input type='hidden' name='id' value="<?php echo $row['id']; ?>">


				<input type="submit" name="submit" value="Обновить отзыв"/>
				<input type="submit" onclick="return confirm('Вы точно хотите удалить этот отзыв?');" name="delete" value="Удалить отзыв"/>
			</form>

			<?php


			

			



			
		}
		$query = '';
		mysqli_free_result($result);
		?>

		
		
		<form id='usrform_cr' class="manage-gallery__form" action="manage_comments_create.php" enctype="multipart/form-data" method="POST">
		<h3>Добавить новый отзыв</h3>
			<div>Текст отзыва.</div>
			<textarea rows="10" cols="80" name="cite" form="usrform_cr"></textarea>
			<div>Автор</div>
			<input type='text' name='author' value=''>

			<input type="submit" name="submit" value="Создать новый отзыв"/>


		</form>
		

	</div>

	</div>

<?php include("../includes/mce.php");?>
</body>
</html>
<?php mysqli_close($connection); ?>