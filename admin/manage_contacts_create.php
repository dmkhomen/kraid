<?php

require_once('../includes/connection.php');

if(isset($_POST['submit']) && !$_POST['header']==''){

    $header = htmlspecialchars($_POST['header']);
    $header = mysqli_real_escape_string($connection,$header);

    $info_1 = htmlspecialchars($_POST['info_1']);
    $info_1 = mysqli_real_escape_string($connection, $info_1);


    $info_2 = htmlspecialchars($_POST['info_2']);
    $info_2 = mysqli_real_escape_string($connection,$info_2);


    $query  = "INSERT INTO contacts (";
    $query .= " header, info_1, info_2 ";
    $query .= ") VALUES (";
    $query .= " '{$header}','{$info_1}','{$info_2}'";
    $query .= ")";

    $result = mysqli_query($connection, $query);

    if ($result) {
        // Success
        // redirect_to("somepage.php");
        //echo "Success!";
        header('Location: manage_contacts.php');
    } else {
        // Failure
        // $message = "Subject creation failed";
        die("Database query failed. " . mysqli_error($connection));
    }
}
//header('Location: manage_contacts.php');