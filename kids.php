<?php require_once("includes/connection.php") ?>
<?php include "header.php" ?>
    <header class="landing-header">
        <a href="index.php">
            <div class="logo link" data-href="banner"></div>
        </a>
        <!--    <div class="slogan">Бути розумним - це круто!</div>-->
        <div class="slogan"><img src="img/cool.gif"></div>
        <!--    <div class="menu-open"></div>-->

        <nav class="menu">
            <input type="checkbox" class="menu-open" name="menu-open" id="menu-open"/>
            <label class="menu-open-button" for="menu-open">
                <span class="hamburger hamburger-1"></span>
                <span class="hamburger hamburger-2"></span>
                <span class="hamburger hamburger-3"></span>
            </label>


            <div class="menu-item"><a href="index.php">Головна</a></div>
            <div class="menu-item"><a href="parents.php">Батькам</a></div>
            <div class="link menu-item" data-href="games">Розваги</div>
            <div class="link menu-item" data-href="contest">Конкурс</div>
            <div class="link menu-item" data-href="colorbooks">Розмальовки</div>
            <!--            <div class="menu-item"><a href="collaboration.php">Співпраця</a></div>-->


        </nav>


    </header>

    <!--[if lt IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a
        href="http://browsehappy.com/">upgrade
        your browser</a> to improve your experience.</p>
    <![endif]-->
    


    <section id="contest" class="landing-section">
        <div class="container">

            <?php $query = "SELECT * FROM blocks WHERE name='contest'";
            $result = mysqli_query($connection, $query);
            if (!$result) {
                die("Something wrong with contest_block database");
            }
            while ($row = mysqli_fetch_assoc($result)){

            ?>


            <h2 class="landing-section__title"><?php echo $row['title']; ?></h2>


            <div class="contest__content">
                <div class="contest__image"><img src="uploads/contest/<?php echo $row['image']; ?>" alt="Конкурс"></div>
                <div class="contest__description"><?php echo htmlspecialchars_decode($row['description']); ?>
                </div>
            </div>
        </div>

        <?php } ?>


<div class="triangle triangle_lower">
            <svg>
                <polygon fill="#FFF698" points="0,100 1583,87 1583,0"></polygon>
            </svg>
        </div>
    </section>

<section id="games" class="landing-section landing-section_games">
        <!-- <div class="triangle triangle_upper">
            <svg>
                <polygon fill="#ABD9F1" points="0,100 100,100 1583,0"></polygon>
            </svg>
        </div> -->
        <div class="container">
            <h2 class="landing-section__title">Розваги</h2>
            <div class="game game_open" data-what="http://kraid.com.ua/date/~game.swf" data-w="760" data-h="400">
                <h3 class="game__title">Чумаки</h3>
                <img src="img/games/game_01.png"></div>
            <div class="game game_open" data-what="http://kraid.com.ua/date/pojar.swf" data-w="760" data-h="400">
                <h3 class="game__title">Пожежник</h3>
                <img src="img/games/game_02.png">
            </div>
            <div class="game game_open" data-what="http://kraid.com.ua/date/swf/lyzhnik.swf" data-w="800" data-h="400">
                <h3 class="game__title">Лижник</h3>
                <img src="img/games/game_03.png">
            </div>


        </div>
        <!-- <div class="triangle triangle_lower">
            <svg>
                <polygon fill="#00A651" points="0,100 1583,87 1583,0"></polygon>
            </svg>
        </div> -->
    </section>

    <section id="colorbooks" class="landing-section landing-section_colorbooks">
         <div class="triangle triangle_upper">
            <svg>
                <polygon fill="#FFF698" points="0,100 100,100 1583,0"></polygon>
            </svg>
        </div>
        <div class="container">
            <h2 class="landing-section__title landing-section__title_bright">Розмальовки</h2>


            <?php
            $query = "SELECT * FROM colorbooks";
            $result = mysqli_query($connection, $query);
            if (!$result) {
                die("Something wrong with colorbooks");
            }
            while ($row = mysqli_fetch_assoc($result)) {

                ?>
                <div class='color-book'>
                    <object data='uploads/colorbooks/<?php echo $row["name"]; ?>' type='application/pdf' width='100%'
                            height='100%'>
                        <iframe src='uploads/colorbooks/<?php echo $row["name"]; ?>' width='100%' height='100%'
                                style='border: none;'>
                            This browser does not support PDFs. Please download the PDF to view it: <a
                                href='uploads/colorbooks/<?php echo $row["name"]; ?>'>Download PDF</a>
                        </iframe>
                    </object>
                </div>

                <?php
            }
            $query = '';
            mysqli_free_result($result);
            ?>


        </div>
    </section>

<?php include "footer.php" ?>
<?php mysqli_close($connection); ?>