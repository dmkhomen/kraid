
<footer class="footer">
    <div class="container">
        <div class="footer__kraid">Професор Крейд - пізнавальний дитячий журнал &copy;
            2005-<?php echo date("Y") ?></div>
        <div class="footer__lightweb">Розробка сайту: <a href="http://lightweb.pp.ua">LightWeb</a></div>
    </div>
</footer>

<!--BEGINmodal -->
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal__content">
        <div class="modal__header">

            <h2>Modal Header</h2>
        </div>
        <span class="modal__close">×</span>
        <div class="modal__body">
            <object type="application/x-shockwave-flash"
                    data="http://kraid.com.ua/date/~game.swf" height="400" width="800">
                <param name="movie" value="http://kraid.com.ua/date/~game.swf">
                <param name="bgcolor" value="#000000">
            </object>
        </div>
    </div>
</div>

<!--ENDmodal-->

<!--BEGINmodalRubric -->
<div id="modal_rubric" class="modal modal_rubric">

    <!-- Modal content -->
    <div class="modal__content">

        <span id="span_rubric" class="modal__close">×</span>
        <div class="modal__body">
            <img src="img/rubrics/academy-kraid_big.png">
        </div>
    </div>
</div>

<!--ENDmodal-->
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>



<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    //    (function (b, o, i, l, e, r) {
    //        b.GoogleAnalyticsObject = l;
    //        b[l] || (b[l] =
    //            function () {
    //                (b[l].q = b[l].q || []).push(arguments)
    //            });
    //        b[l].l = +new Date;
    //        e = o.createElement(i);
    //        r = o.getElementsByTagName(i)[0];
    //        e.src = 'https://www.google-analytics.com/analytics.js';
    //        r.parentNode.insertBefore(e, r)
    //    }(window, document, 'script', 'ga'));
    //    ga('create', 'UA-119295-16', 'auto');
    //    ga('send', 'pageview');
</script>
</body>
</html>