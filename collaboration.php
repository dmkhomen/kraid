<?php include "header.php"; ?>
<!-- TODO ANNA what about this page, how does this need to look like?-->
<header class="landing-header">
    <a href="index.php"><div class="logo link" data-href="banner"></div></a>
    <!--    <div class="slogan">Бути розумним - це круто!</div>-->
    <div class="slogan"><img src="img/cool.gif"></div>
    <!--    <div class="menu-open"></div>-->

    <nav class="menu">
        <input type="checkbox" class="menu-open" name="menu-open" id="menu-open"/>
        <label class="menu-open-button" for="menu-open">
            <span class="hamburger hamburger-1"></span>
            <span class="hamburger hamburger-2"></span>
            <span class="hamburger hamburger-3"></span>
        </label>
        

        <div class="menu-item"><a href="index.php">Головна</a></div>

        <div class="menu-item"><a href="parents.php">Батькам</a></div>
        <div class="menu-item"><a href="kids.php">Дітям</a></div>



    </nav>


</header>



<section class="landing-section landing-section_collaboration">
    <div class="container">
        <h2 class="landing-section__title">Співпраця</h2>
        
        <div class="collaboration-item">
            <h3 class="collaboration__title">Художникам</h3>
            <p><img class="collaboration__image" src="img/artists.png">Яскравий та сучасний журнал неможливий без гарних малюнків! Тож «Професор Крейд» із задоволенням співпрацює із новими художниками й охоче переглядає усі надіслані портфоліо. Якщо робота сподобалася, редакція контактує з художником, обговорює усі деталі і дає цікавезне завдання.<br>
                Після виходу журналу художнику надсилається авторський примірник і виплачується гонорар.
            </p>
            
            <p><b>Основні побажання:</b><br>
                1. Вітається, якщо художник заздалегідь ознайомиться з журналом і матимете уявлення про специфіку видання та ілюстрації рубрик. Пам’ятайте, те, що підходить для ілюстрування книжок, не завжди доречне у журналі.<br>
                2. Редакція приймає роботи до розгляду у будь-яких форматах, а також у вигляді посилань на портфоліо.<br>
                3. Трапляється, що редакція може бути не впевнена щодо виконання художником ілюстрації. Тоді пропонується виконати пробний малюнок. Таке завдання не оплачується, але і вимоги до нього менші: без термінів виконання та постановки сюжету.<br>
                4. Обов’язково вказуйте свої координати, щоб редакція могла з вами зв’язатися.
            </p>
            <p>Приклади своїх робіт надсилайте на електронну адресу <a href="mailto:professor@kraid.com.ua">professor@kraid.com.ua</a>.<br>
                Натхнення вам!
            </p>
        </div>
        <div class="collaboration-item">
            <h3 class="collaboration__title">Авторам</h3>
            <p><img class="collaboration__image" src="img/writers.png">«Професор Крейд» і вся його команда радо запрошує до співпраці талановитих авторів, які пишуть дитячі художні розповіді, оповідання, комікси. Редакція залюбки розгляне ваші рукописи. Якщо твір сподобається і вразить оригінальністю сюжету, то буде прийнято рішення опублікувати його в одному з номерів.<br>
                І тільки після прийняття остаточного рішення, редакція зв’яжеться з автором для обговорення усіх деталей.<br>
                Після виходу номеру з друку автору надсилається примірник і видається гонорар.
            </p>
            <p><b>УВАГА!</b> До редакції надходить величезна купа листів від читачів, партнерів і всіх бажаючих співпрацювати, тож просимо вас з розумінням поставитися до того, що редакція НЕ рецензує рукописи, НЕ коментує їх та НЕ відповідає на кожен лист щодо його отримання або чому рукопис не підійшов. Також ми НЕ повертаємо рукописи.<br>
                Твори приймаються лише в електронному вигляді й обов’язково з координатами автора: ПІБ, контактний телефон, електронна та поштова адреси.
            </p>
            <p><b>Основні вимоги до творів:</b><br>
                1. Теми, що захоплюють сучасних дітей віком від 6 до 10 років.<br>
                2. Гумор. Динамічність. Позитивність.<br>
                3. Написано для дітей і про дітей.<br>
            </p>
            <p>Не злякалися наших вимог? Тоді надсилайте свої твори до редакції на електронну адресу <a href="mailto:professor@kraid.com.ua">professor@kraid.com.ua</a>.<br>
                «Професор Крейд» чекає на ваші твори і, можливо, до друку обере саме ваш рукопис! 
                Тож успіхів усім і кожному!
            </p>
        </div>

    </div>
</section>
<?php include "footer.php"; ?>