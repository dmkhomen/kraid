-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 28, 2016 at 05:06 PM
-- Server version: 5.7.16
-- PHP Version: 5.6.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kraid_landing`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`) VALUES
(1, 'adminkraid', '$2y$10$NmJlOGNjMzU2OGQxZTRkM.xUhM032djDFpRGpVepQMKQH.NoDDmUm');

-- --------------------------------------------------------

--
-- Table structure for table `blocks`
--

CREATE TABLE `blocks` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `blocks`
--

INSERT INTO `blocks` (`id`, `name`, `title`, `description`, `image`) VALUES
(1, 'main', 'Професор Крейд', 'Яскравий, динамічний, сучасний пізнавально-розважальний журнал для дітей 6-10 років. Журнал цікаво та доступно розповідає про навколишній світ, дивовижні природні явища та складні наукові поняття. Юні читачі поринуть у світ надзвичайних відкриттів, прикольних дослідів, розумних ігор, розваг та гумористичних коміксів і переконаються, що бути розумним — це круто!', 'main.jpg'),
(2, 'announcement', 'Новий випуск вже у продажу!', '&lt;p&gt;\r\nФізкульт-привіт усім читачам від чергового випуску журналу «Професор Крейд»!&lt;br&gt;Новий номер здивує вас безліччю цікавинок: коли і навіщо придумали Олімпійські ігри? З чого виготовляють медалі для спортсменів? Чи можна стати рекордсменом, якщо ти ще дитина? А ще дізнавайтеся, що таке мімікрія, вирушайте до пустельної савани Африки, щоб познайомитися з доброзичливим каракалом, а звідти гайда до запальної Іспанії, де професор пояснить, що пов’язує Канарські острови з… собаками. А потім довідайтеся, як завжди залишатися на зв’язку та чи можна надрукувати собі сніданок. Дива та й годі! І це ще не все.\r\n&lt;/p&gt;\r\n\r\n&lt;p&gt;\r\nСтаньте справжніми дослідниками та знайдіть заховане шкільне приладдя разом із «Шукачем у школі» та заповнюйте розклад на новий семестр. А ще знайомтеся з читачами журналу у фотоальбомі професора Крейда і долучайтеся до флешмобу.\r\n&lt;/p&gt;\r\n\r\n&lt;p&gt;\r\nПро все це і навіть більше читайте на сторінках чемпіонського випуску.\r\n&lt;/p&gt;', 'journal.jpg'),
(3, 'contest', 'Долучайся до флешмобу «Фотоальбом професора Крейда»!', '&lt;p&gt;Друже, хочеш потрапити до фотоальбому професора? Так? А нам кортить тебе побачити!&lt;/p&gt;\r\n&lt;p&gt;Сфотографуйся разом із журналом, а ще краще &amp;ndash; з цілою добіркою номерів &amp;laquo;Професора Крейда&amp;raquo;. Вигадай прикольний напис. Надішли фото з підписом на електронну адресу professor@kraid.com.ua.&lt;/p&gt;\r\n&lt;p&gt;Фотографія повинна бути якісною, чіткою, яскравою та бажано не менше 2 Мп.&lt;br /&gt;Не забудь вказати своє ім&amp;rsquo;я, прізвище, скільки тобі років і де мешкаєш.&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Найцікавіші фото будуть надруковані на сторінках журналу!&lt;/strong&gt;&lt;/p&gt;', 'contest.png');

-- --------------------------------------------------------

--
-- Table structure for table `colorbooks`
--

CREATE TABLE `colorbooks` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `colorbooks`
--

INSERT INTO `colorbooks` (`id`, `name`) VALUES
(14, 'Археологи.pdf'),
(15, 'Відпочинок.pdf'),
(16, 'Крейд.pdf'),
(17, 'Смик.pdf'),
(18, 'Шила.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `cite` text NOT NULL,
  `author` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `cite`, `author`) VALUES
(6, '&lt;p&gt;Я, коли ще була дуже маленькою, дуже полюбляла читати ваш журнал))&amp;nbsp;Він був дуже цікавим і багато чого мене навчив! Дякую вам за чудові спогади і за все що я від вас навчилась)))&lt;/p&gt;', 'Луїза'),
(7, 'Я дізнався про цей журнал, коли мені його подарували, і читаю його вже три роки. Найбільше подобаються в журналі комікси, тому що вони дуже смішні й веселі. Я ще хочу прочитати багато коміксів. А мій люблений герой в журналі – професор Крейд.', 'Міша, 9 років, м. Київ'),
(8, 'Хочу подякувати за чудовий журнал, який ми з донечкою постійно купуємо, і вона із задоволенням читає та виконує ваші завдання!', 'Інна, м. Київ'),
(10, 'Дякую вам за крутезний журнал для діток. Моя першокласниця у шкільній бібліотеці познайомилася з «Професором Крейдом» і тепер виявляє неабияке бажання мати всю підшивку. Ще раз вам дякую і багато-пребагато читачів.', 'Адріана, м. Львів'),
(11, 'Дуже дякую Вам. Ваш журнал супер. Ми його читали, читаємо і будемо читати! Гарних Вам ідей!', 'Алла, м. Київ'),
(12, 'Мій син зачитується журналом з 4-х років і настільки захоплений ним, що ми з чоловіком свого часу організували для нього квест від професора. Синок отримав лист із мапою та завданнями, які виконувалися потім усією родиною. Правда тепер він постійно питає, коли ж професор напише йому знову :) Тож дякуємо колективу журналу за цікавий час для дитини. ', 'Марина, м. Київ'),
(13, '&lt;p&gt;Гарний він такий один&lt;/p&gt;\r\n&lt;p&gt;І рудий, мов апельсин:&lt;/p&gt;\r\n&lt;p&gt;Хвіст, спина, живіт і вушка,&lt;/p&gt;\r\n&lt;p&gt;Хтось іще &amp;laquo;накреслив&amp;raquo; смужки.&lt;/p&gt;\r\n&lt;p&gt;Полюбляє він науки!&lt;/p&gt;\r\n&lt;p&gt;Ти не знатимеш з ним скуки,&lt;/p&gt;\r\n&lt;p&gt;Бо до жартів він так звик,&lt;/p&gt;\r\n&lt;p&gt;Хоч у Крейда помічник&lt;/p&gt;\r\n&lt;p&gt;І працює у журналі.&lt;/p&gt;\r\n&lt;p&gt;Мабуть, Смика всі впізнали!&lt;/p&gt;', 'Микита, м. Харків'),
(14, '&lt;p&gt;Мій син уже підліток, але ще й досі із задоволенням слухає чарівні радіоефіри на дисках з журналів, які ми купували для нього, коли він був ще у початковій школі. Насправді і я їх люблю і мені цікаво їх слухати. Колись давно в дитинстві ми слухали платівки з казками, так само і ваші радіоефіри полюбились нам дуже і дуже сильно.&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Дякую Вам за такі чудові ефіри, цікаву інформацію. Особливо хочу подякувати за музичну передачу та етикетку. Мій син навчається в музичній школі і ці передачі дуже йому допомогли на уроках муз літератури і для гармонійного розвитку необхідні.&lt;/p&gt;\r\n&lt;p&gt;ДЯКУЮ, ДЯКУЮ, ДЯКУЮ.&lt;/p&gt;', 'Наталія М.');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `header` varchar(255) NOT NULL,
  `info_1` varchar(255) NOT NULL,
  `info_2` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `header`, `info_1`, `info_2`) VALUES
(6, 'Адреса для листування', 'а/с 55, м. Київ, 03110, Україна', ''),
(7, 'Фактична адреса редакції', 'вул. Пироговського, 19/1, м. Київ, 03110', ''),
(8, 'Телефон редакції', '(044) 595-53-10', ''),
(9, 'Відділ редакційної передплати', '(044) 595-53-13', ''),
(10, 'E-mail:', 'professor@kraid.com.ua', ''),
(11, 'Шеф-редактор', 'Анна Масюк', 'amasyuk@companion.ua'),
(12, 'Головний редактор', 'Анна Шевченко', 'annashevchenko@companion.ua'),
(13, 'Відділ реклами', 'Вікторія Анісімова', 'anisimova@companion.ua'),
(14, 'googlemaps', '50.419629', '30.478017');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_photos`
--

CREATE TABLE `gallery_photos` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `gallery_photos`
--

INSERT INTO `gallery_photos` (`id`, `name`) VALUES
(14, '1.jpg'),
(15, '2.jpg'),
(16, '3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `partner_logos`
--

CREATE TABLE `partner_logos` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `partner_logos`
--

INSERT INTO `partner_logos` (`id`, `name`) VALUES
(6, 'artek.png'),
(7, 'BrownLogo_New7.png'),
(8, 'logo detsad.jpg'),
(9, 'logo.png'),
(10, 'Logo_Dytyacha Planeta_2016.jpg'),
(11, 'Olkom Logo copy.jpg'),
(12, 'медична компанія.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `youtube_videos`
--

CREATE TABLE `youtube_videos` (
  `id` int(11) NOT NULL,
  `video_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `youtube_videos`
--

INSERT INTO `youtube_videos` (`id`, `video_id`) VALUES
(1, 'NoYBjNYi-1E');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blocks`
--
ALTER TABLE `blocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colorbooks`
--
ALTER TABLE `colorbooks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_photos`
--
ALTER TABLE `gallery_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partner_logos`
--
ALTER TABLE `partner_logos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `youtube_videos`
--
ALTER TABLE `youtube_videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `blocks`
--
ALTER TABLE `blocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `colorbooks`
--
ALTER TABLE `colorbooks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `gallery_photos`
--
ALTER TABLE `gallery_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `partner_logos`
--
ALTER TABLE `partner_logos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `youtube_videos`
--
ALTER TABLE `youtube_videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
