<?php 
require_once('includes/connection.php');
include "header.php";
?>
<header class="landing-header">
    <div class="logo link" data-href="banner"></div>
    <!--    <div class="slogan">Бути розумним - це круто!</div>-->
    <div class="slogan"><img src="img/cool.gif"></div>
    <!--    <div class="menu-open"></div>-->

    <nav class="menu">
        <input type="checkbox" class="menu-open" name="menu-open" id="menu-open"/>
        <label class="menu-open-button" for="menu-open">
            <span class="hamburger hamburger-1"></span>
            <span class="hamburger hamburger-2"></span>
            <span class="hamburger hamburger-3"></span>
        </label>
        <!--    <div data-where="#contact" class="menu-item"> Связаться</div>-->
        <!--    <div data-where="#feedback" class="menu-item"> Отзывы</div>-->
        <!--    <div data-where="#portfolio" class="menu-item"> Примеры</div>-->
        <!--    <div data-where="#services" class="menu-item"> Услуги</div>-->
        <!--    <div data-where="#about" class="menu-item"> Обо мне</div>-->
        <!--    <div data-where="#home" class="menu-item"> LightWeb</div>-->

        <div class="link menu-item" data-href="banner">Головна</div>
        <div class="link menu-item" data-href="journal">Журнал</div>
        <div class="link menu-item" data-href="heroes">Наші герої</div>
        <div class="link menu-item" data-href="rubrics">Рубрики</div>
        <!--        <div class="link menu-item" data-href="comments">Відгуки</div>-->
        <!--        <div class="link menu-item" data-href="children">Розваги</div>-->
        <div class="link menu-item" data-href="parentsandkids">Батькам</div>
        <div class="link menu-item" data-href="parentsandkids">Дітям</div>
        <div class="link menu-item" data-href="socials">Ми у соцмережах</div>
        <div class="link menu-item" data-href="gallery">Фотогалерея</div>
        <!--        <div class="link menu-item" data-href="subscription">Як отримати?</div>-->
        <div class="link menu-item" data-href="contacts">Контакти</div>

        <div class="menu-item"><a href="collaboration.php">Співпраця</a></div>


    </nav>


</header>


<!-- Add your site or application content here -->
<div id="fb-root"></div>
<script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<?php $query = "SELECT * FROM blocks WHERE name='main'";
$result = mysqli_query($connection, $query);
if (!$result){
    die("Something wrong with main_block database");
}
while($row = mysqli_fetch_assoc($result)){

    ?>
    <section id="banner" class="landing-section landing-section_banner"
    style="background-image: url('uploads/main/<?php echo $row['image'];?>')">
    <div class="container">
        <div class="landing-section__info">



            <h1 class="landing-section__title"><?php echo $row['title'];?></h1>
            <p class="landing-section__description"><?php echo htmlspecialchars_decode($row['description']);?></p>
            <div class="banner-buttons">
                <div class="button"><a href="parents.php">Замовити</a></div>
                <div class="button button_last link" data-href="journal">Дізнатись більше</div>
            </div>
        </div>
        <?php } ?>
        <div class="landing-section__presentation">
            <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" style="width:100%; height:100%; "
            id="dc681c77-116c-80fe-c6b3-cef5c26c1954">
            <param name="movie"
            value="http://static.issuu.com/webembed/viewers/style1/v2/IssuuReader.swf?mode=mini&amp;autoFlip=true&amp;embedBackground=%23ffffff&amp;backgroundColor=%23222222&amp;documentId=101222113738-5035dd7fc35c458486d546c589935f9f&amp;autoPlay=true">
            <param name="allowfullscreen" value="true">
            <param name="menu" value="false">
            <param name="wmode" value="transparent">
            <embed src="http://static.issuu.com/webembed/viewers/style1/v2/IssuuReader.swf"
            type="application/x-shockwave-flash"
            allowfullscreen="true" menu="false" wmode="transparent" style="width:100%; height:100%; "
            flashvars="mode=mini&amp;autoFlip=true&amp;embedBackground=%23ffffff&amp;backgroundColor=%23222222&amp;documentId=101222113738-5035dd7fc35c458486d546c589935f9f&amp;autoPlay=true"
            title="Adobe Flash Player">
        </object>
    </div>
</div>
</section>
<section id="journal" class="landing-section landing-section_journal">
    <div class="container">

        <?php $query = "SELECT * FROM blocks WHERE name='announcement'";
        $result = mysqli_query($connection, $query);
        if (!$result){
            die("Something wrong with announcement_block database");
        }
        while($row = mysqli_fetch_assoc($result)){

            ?>
            <h2 class="landing-section__title"><?php echo $row['title'];?></h2>
            <div class="new-journal">

                <div class="new-journal__image"><img
                    src="uploads/announcement/<?php echo $row['image'];?>" alt="Свіжий номер"></div>
                    <div class="new-journal__description"><?php echo htmlspecialchars_decode($row['description']);?>
                    </div>
                </div>
            </div>
            <?php } ?>
        </section>
        <section id="heroes" class="landing-section landing-section_heroes">
            <div class="triangle triangle_upper">
                <svg>
                    <polygon fill="#ffffff" points="0,0 0,87 1583,0"></polygon>
                </svg>
            </div>
            <div class="container">
                <h2 class="landing-section__title landing-section__title_bright">Наші герої</h2>
                <div class="heroes">
                    <div class="hero">
                        <div class="hero__image"><img src="img/kraid.gif"></div>
                        <!--                <div class="hero__name">Професор Крейд</div>-->
                        <div class="hero__description">Академік усіх існуючих наук. Знає все про все і навіть більше, ніж усе.
                            На жаль, розумна професорова голова просто не встигає думати про речі, простіші від гравітації чи
                            молекул, тому він часто потрапляє в дивні ситуації.
                        </div>
                        <div class="hero__likes"><span>Уподобання: </span>Слухати музику, подорожувати та робити винаходи.</div>
                        <div class="hero__dislikes"><span>Не подобається: </span>Коли комусь нудно. З професором нудно не буває
                            нікому.
                        </div>
                    </div>
                    <div class="hero hero_bg2">
                        <div class="hero__image"><img src="img/minzurka.gif"></div>
                        <!--                <div class="hero__name">Мензурка</div>-->
                        <div class="hero__description">Відданий фанат та помічник професора Крейда. Ось хто думає про буденне,
                            доки професор зайнятий наукою. Мензурка завжди напоїть його чаєм та героїчно випробує на собі
                            будь-який новий винахід.
                        </div>
                        <div class="hero__likes"><span>Уподобання: </span>Книжки, спорт та досліди.</div>
                        <div class="hero__dislikes"><span>Не подобається: </span>Якщо після випробувань у нього щось болить.
                        </div>
                    </div>
                    <div class="hero hero_bg3">
                        <div class="hero__image"><img src="img/smik.gif"></div>
                        <!--                <div class="hero__name">Кіт Смик</div>-->
                        <div class="hero__description">Суцільна шкода. Коли не дуже зайнятий їжею чи сном, Смик завжди радий
                            докласти своїх смугастих лап до того, що котам чіпати суворо заборонено. Просто так, аби подивитися,
                            що воно буде.
                        </div>
                        <div class="hero__likes"><span>Уподобання: </span>Холодильники, повні їжі. Молочні та м’ясні магазини.
                        </div>
                        <div class="hero__dislikes"><span>Не подобається: </span>Відповідати за скоєні вчинки.
                        </div>
                    </div>
                    <div class="hero hero_bg4">
                        <div class="hero__image"><img src="img/shula.gif"></div>
                        <!--                <div class="hero__name">Миша Шила</div>-->
                        <div class="hero__description">Колись звичайна лабораторна біла миша. Трансформувалася в результаті
                            таємничого експерименту... та це окрема історія. Проте тепер у неї на хвостику постійно спалахує
                            іскра. Розумна, але довірлива та сором’язлива. Можливо, саме тому Шила досі не здобула наукового
                            звання.
                        </div>
                        <div class="hero__likes"><span>Уподобання: </span>Все нове та цікаве.</div>
                        <div class="hero__dislikes"><span>Не подобається: </span>Коти (окрім Смика, звичайно).
                        </div>
                    </div>
                </div>
            </div>
            <div class="triangle triangle_lower">
                <svg>
                    <polygon fill="#ffffff" points="0,100 1583,87 1583,0"></polygon>
                </svg>
            </div>
        </section>

        <section id="rubrics" class="landing-section landing-section_rubrics">
            <div class="container">
                <h2 class="landing-section__title">Рубрики журналу</h2>
                <div class="rubric rubric_academy-kraid">
                    <div class="rubric__image"><img src="img/rubrics/academy-kraid.png"></div>
                    <div class="rubric__text">
                        <h3 class="rubric__title">Академія Крейда</h3>
                        <div class="rubric__description">Захоплююче про навколишній світ, складні наукові поняття, природні
                            явища та процеси, що відбуваються довкола.
                        </div>
                    </div>
                </div>
                <div class="rubric rubric_watch">
                    <div class="rubric__image"><img src="img/rubrics/watch.png"></div>
                    <div class="rubric__text">
                        <h3 class="rubric__title">Розглядалка</h3>
                        <div class="rubric__description">Яскрава та оригінальна гра-малюнок, яка розвиває спостережливість,
                            уважність і посидючість.
                        </div>
                    </div>
                </div>
                <div class="rubric rubric_eureka">
                    <div class="rubric__image"><img src="img/rubrics/eureka.png"></div>
                    <div class="rubric__text">
                        <h3 class="rubric__title">Еврика</h3>
                        <div class="rubric__description">Доступно про механізми та процеси виробництва товарів, продуктів і
                            речей, а також про принципи роботи пристроїв.
                        </div>
                    </div>
                </div>
                <div class="rubric rubric_playground">
                    <div class="rubric__image"><img src="img/rubrics/playground.png"></div>
                    <div class="rubric__text">
                        <h3 class="rubric__title">Ігротека</h3>
                        <div class="rubric__description">Різноманітні ігрові завдання для розвитку логічного та образного
                            мислення.
                        </div>
                    </div>
                </div>
                <div class="rubric rubric_live-world">
                    <div class="rubric__image"><img src="img/rubrics/live-world.png"></div>
                    <div class="rubric__text">
                        <h3 class="rubric__title">Живосвіт</h3>
                        <div class="rubric__description">Цікаві факти з життя тварин.
                        </div>
                    </div>
                </div>
                <div class="rubric rubric_workshop">
                    <div class="rubric__image"><img src="img/rubrics/workshop.png"></div>
                    <div class="rubric__text">
                        <h3 class="rubric__title">Майстерня Крейда</h3>
                        <div class="rubric__description">Створення оригінальних виробів своїми руками.
                        </div>
                    </div>
                </div>
                <div class="rubric rubric_english">
                    <div class="rubric__image"><img src="img/rubrics/english.png"></div>
                    <div class="rubric__text">
                        <h3 class="rubric__title">English with fun</h3>
                        <div class="rubric__description">Вивчення англійської мови в ігровій формі.
                        </div>
                    </div>
                </div>
                <div class="rubric rubric_what-this">
                    <div class="rubric__image"><img src="img/rubrics/what-this.png"></div>
                    <div class="rubric__text">
                        <h3 class="rubric__title">Що це?</h3>
                        <div class="rubric__description">Фотозагадка, за допомогою якої пояснюється незвичайне явище чи об’єкт.
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="parentsandkids" class="landing-section landing-section_parentsandkids">
            <div class="container">

                <a href="parents.php" class="parentsandkids-item parentsandkids-item_parents">
                    <div>
                        <h3 class="parentsandkids-item__title landing-section__title">Батькам</h3>
                    </div>
                </a>
                <a href="kids.php" class="parentsandkids-item parentsandkids-item_kids">
                    <div>
                        <h3 class="parentsandkids-item__title landing-section__title">Дітям</h3>
                    </div>
                </a>
            </div>
        </section>

        <section id="socials" class="landing-section landing-section_socials">
            <!--    <div class="triangle triangle_upper">-->
            <!--        <svg>-->
            <!--            <polygon fill="#ffffff" points="0,0 0,87 1583,0"></polygon>-->
            <!--        </svg>-->
            <!--    </div>-->
            <div class="social-item social-item_youtube">
                <h2 class="landing-section__title landing-section__title_youtube">Ми на <span
                    class="another-font">YouTube</span></h2>
                    <!--TODO get youtube url from DB and echo here -->
                    <?php 

                    $query = "SELECT * FROM youtube_videos";
                    $result = mysqli_query($connection, $query);
                    if (!$result){
                        die("Something wrong with video database");
                    }
                    while($row = mysqli_fetch_assoc($result)){
                        echo "<iframe class='youtube__iframe' width='360' height='270'
                        src='https://www.youtube.com/embed/{$row["video_id"]}'>
                    </iframe>";


                }
                $query = '';
                mysqli_free_result($result);
                ?>
            </div>
            <div class="social-item social-item_facebook">
                <h2 class="landing-section__title landing-section__title_facebook">Сторінка у Facebook</h2>
                <div class="fb-page" data-href="https://www.facebook.com/ProffesorKraid/" data-tabs="timeline" data-width="360"
                data-height="270" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false"
                data-show-facepile="true">
                <blockquote cite="https://www.facebook.com/ProffesorKraid/" class="fb-xfbml-parse-ignore"><a
                    href="https://www.facebook.com/ProffesorKraid/">Професор Крейд</a></blockquote>
                </div>
            </div>
        </section>
        <section id="gallery" class="landing-section landing-section_gallery">



            <div class="gallery">

             <?php
             $query = "SELECT * FROM gallery_photos";
             $result = mysqli_query($connection, $query);
             if (!$result){
                die("Something wrong with gallery");
            }
            while($row = mysqli_fetch_assoc($result)){
                echo "	<div class='gallery__item' style='background-image: url(uploads/gallery/" .
                $row['name'] . ");'></div>";
            }
            $query = '';
            mysqli_free_result($result);
            ?>


        </div>



    </section>

    <section id="contacts" class="landing-section landing-section_contacts">

        <div class="container">
            <div class="contacts">
                <!--TODO php get contacts from database and echo them-->

                <?php

                $query = "SELECT * FROM contacts ";
                $result = mysqli_query($connection, $query);
                if (!$result){
                    die("Something wrong with contest_block database");
                }
                while($row = mysqli_fetch_assoc($result)){

                    if($row['header']=='googlemaps'){
                        $ggl_lat = $row['info_1'];
                        $ggl_lon = $row['info_2'];

                    } else {

                        ?>



                        <div class="contact">
                            <div class="contact__title"><?php echo $row['header']; ?></div>
                            <div class="contact__info"><?php echo $row['info_1']; ?></div>
                            <div class="contact__info"><?php echo $row['info_2']; ?></div>
                        </div>



                        <?php 
                    }
                }
                $query = '';
                mysqli_free_result($result);
                ?>



                <div class="contact contact_soc">
                    <a href="https://www.youtube.com/channel/UCaHDni4FT6BVk9so7gNcVAw">
                        <div class="contact__icon contact__icon_youtube"></div>
                    </a>
                    <a href="https://www.facebook.com/ProffesorKraid">
                        <div class="contact__icon contact__icon_facebook"></div>
                    </a>
                </div>
            </div>
            <div id="map"></div>

        </div>

        <div class="partners">
            <div class="container">
                <h2 class="landing-section__title landing-section__title_partners">Наші партнери</h2>
                <!--TODO php get images for partners from uploads/partners and echo them-->
                <!--TODO ANNA receive partners images-->

                <?php
                $query = "SELECT * FROM partner_logos";
                $result = mysqli_query($connection, $query);
                if (!$result){
                    die("Something wrong with partners");
                }
                while($row = mysqli_fetch_assoc($result)){
                    echo "<div class='partner'><img src='uploads/partners/{$row["name"]}'></div>";
                }
                $query = '';
                mysqli_free_result($result);
                ?>


            </div>
        </div>
    </section>


    <div class="toTop link" data-href="banner"></div>

    <script>
        function myMap() {
            var mapCanvas = document.getElementById("map");
            var myCenter = new google.maps.LatLng(<?php echo $ggl_lat ?>, <?php echo $ggl_lon ?>);
            var mapOptions = {
                center: new google.maps.LatLng(<?php echo $ggl_lat ?>, <?php echo $ggl_lon ?>),
                zoom: 17,
                disableDefaultUI: false
            };
            var map = new google.maps.Map(mapCanvas, mapOptions);
            var marker = new google.maps.Marker({position: myCenter});
            marker.setMap(map);
        }
    </script>
    <script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCotB7u2KosDCiFK10sZ-rCuP07XuXXDA&callback=myMap&language=uk"></script>
    <?php include "footer.php" ?>
    <?php mysqli_close($connection); ?>