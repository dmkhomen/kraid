<?php require_once('includes/connection.php');
include "header.php" ?>
<header class="landing-header">
    <a href="index.php"><div class="logo link" data-href="banner"></div></a>
    <!--    <div class="slogan">Бути розумним - це круто!</div>-->
    <div class="slogan"><img src="img/cool.gif"></div>
    <!--    <div class="menu-open"></div>-->

    <nav class="menu">
        <input type="checkbox" class="menu-open" name="menu-open" id="menu-open"/>
        <label class="menu-open-button" for="menu-open">
            <span class="hamburger hamburger-1"></span>
            <span class="hamburger hamburger-2"></span>
            <span class="hamburger hamburger-3"></span>
        </label>
        <!--    <div data-where="#contact" class="menu-item"> Связаться</div>-->
        <!--    <div data-where="#feedback" class="menu-item"> Отзывы</div>-->
        <!--    <div data-where="#portfolio" class="menu-item"> Примеры</div>-->
        <!--    <div data-where="#services" class="menu-item"> Услуги</div>-->
        <!--    <div data-where="#about" class="menu-item"> Обо мне</div>-->
        <!--    <div data-where="#home" class="menu-item"> LightWeb</div>-->

        <div class="menu-item"><a href="index.php">Головна</a></div>
        <div class="menu-item"><a href="kids.php">Дітям</a></div>
    
        <div class="link menu-item" data-href="subscription">Як отримати?</div>
        <div class="link menu-item" data-href="comments">Відгуки</div>
        <!--        <div class="menu-item"><a href="collaboration.php">Співпраця</a></div>-->



    </nav>


</header>


<!--[if lt IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
    <![endif]-->

    <?php //include "presentation.php" ?>

    <section id="subscription" class="landing-section landing-section_subscription">
        <div class="container">
            <h2 class="landing-section__title">Як отримати журнал?</h2>
            <div class="subscription__why">Передплатіть улюблений журнал своєї дитини вже зараз і вона матиме 11 унікальних номерів, не пропускатиме жодного конкурсу, щомісяця читатиме про нові пригоди дружної команди професора Крейда, дізнаватиметься нове та цікаве і просто класно проводитиме час. І ваша дитина точно дізнається, що бути розумним – це круто!
            </div>
            <div style='margin-top: 1em;'><b>Оформити передплату на журнал можна:</b></div>
            <div class="subscription__method">На офіційному сайті-магазині <a
                href="http://shop.companion.ua/index.php/category/catlist/44">shop.companion.ua</a>;
            </div>
            <div class="subscription__method">За допомогою <a
                href="http://www.portmone.com.ua/v2/ru/services/periodicals/3608/">www.portmone.com</a>;
            </div>
            <div class="subscription__method">Поспілкувавшись з редакцією за телефоном (044) 595-53-13;</div>
            <div class="subscription__method">Або у будь-якому відділенні Укрпошти - передплатний індекс журналу 91413.
            </div>
            <div style='margin-top: 1em;'><b>Наголошуємо, що в редакції ви маєте можливість оформити передплату з будь-якого місяця.</b></div>
            <!-- <div class="subscription__image"></div> -->
        </div>
    </section>


    <section id="comments" class="landing-section landing-section_comments">
        <div class="triangle triangle_upper">
            <svg>
                <polygon fill="#FFD300" points="0,0 0,87 1583,0"></polygon>
            </svg>
        </div>
        <div class="container">
            <h2 class="landing-section__title">Відгуки</h2>
            
                <!-- TODO get comments&authors from database and echo them here            -->

                <?php

                $query = "SELECT * FROM comments ";
                $result = mysqli_query($connection, $query);
                if (!$result){
                    die("Something wrong with contest_block database");
                }
                while($row = mysqli_fetch_assoc($result)){



                    ?>





                    <div class="comment">
                        <div class="comment__icon"></div>
                        <div class="comment__text">
                            <div class="comment__description"><?php echo htmlspecialchars_decode($row['cite']); ?></div>
                            <div class="comment__author"><?php echo htmlspecialchars_decode($row['author']); ?></div>
                        </div>
                    </div>



                    <?php 
                    
                }
                $query = '';
                mysqli_free_result($result);
                ?>




            
        </div>
        <!--    <div class="triangle triangle_lower">-->
        <!--        <svg>-->
        <!--            <polygon fill="#FFD300" points="0,100 1583,87 1583,0"></polygon>-->
        <!--        </svg>-->
        <!--    </div>-->
    </section>

    <?php include "footer.php" ?>